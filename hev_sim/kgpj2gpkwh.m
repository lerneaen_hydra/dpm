function gpkwh = kgpj2gpkwh(kgpj)
%KGPJ2GPKWH Scales power from Kg/J to g/kWh
gpkwh = kgpj * (60 * 60 * 1000 * 1000);

end

