function kgpj = gpkwh2kgpj(gpkwh)
%GPKWH2KGPJ Scales power from g/kWh to Kg/J
kgpj = gpkwh / (60 * 60 * 1000 * 1000);


end

